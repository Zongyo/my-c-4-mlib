import os
import shutil
from settings import SETTINGS

version = SETTINGS['version']
target = 'dist\\c4mlib-v'+ version +'.pdf'

def makePdf():
    cwd = os.getcwd()
    os.system('make -C docs latex')
    infilename = 'docs/build/latex/c4mlib.tex'
    outfilename = 'c4mlib.tex'
    with open(outfilename, 'w', encoding='utf-8') as fout, open(infilename, 'r', encoding='utf-8') as fin:
        for line in fin:
            if line == '\\usepackage[utf8]{inputenc}\n':
                fout.write(line)
                fout.write('\\usepackage{CJKutf8}\n')
            elif line == '\\urlstyle{same}\n':
                fout.write(line)
                fout.write('\\begin{CJK}{UTF8}{bsmi}\n')
            elif line == '\\author{MVMC-lab}\n':
                fout.write(line)
                fout.write('\\end{CJK}\n')
            elif line == '\\begin{document}\n':
                fout.write(line)
                fout.write('\\begin{CJK}{UTF8}{bsmi}\n')
            elif line == '\\end{document}':
                fout.write('\\end{CJK}\n')
                fout.write(line)
            else:
                fout.write(line)
    shutil.move(outfilename, infilename)
    os.system('make -C docs\\build\\latex')
    shutil.move('docs\\build\\latex\\c4mlib.pdf', target)
    os.chdir(cwd)

def makeHtml():
    cwd = os.getcwd()
    os.makedirs('dist', exist_ok=True)
    os.chdir('docs')
    os.system('doxygen')
    os.system('make html')
    os.chdir(cwd)

def run():
    shutil.rmtree('docs\\build\\', ignore_errors=True)
    makeHtml()
    makePdf()

if __name__ == '__main__':
    run()
