#include "c4mlib.h"

int main() {
    C4M_DEVICE_set();

    float data[2][5] = {
        {0, 1, 2, 3, 4},
        {5, 6, 7, 8, 9}
    };

    uint8_t res;

    res = HMI_put_matrix(HMI_TYPE_F32, 2, 5, data);

    printf("res is %d\n", res);

    return 0;
}
