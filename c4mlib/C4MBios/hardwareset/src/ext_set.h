/**
 * @file ext_set.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2020.08.17
 * @brief 
 * 
 */

#ifndef C4MLIB_HARDWARESET_EXT_SET_H
#define C4MLIB_HARDWARESET_EXT_SET_H

#if defined(__AVR_ATmega128__)
#    include "m128/ext_set.h"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif

#endif  // C4MLIB_HARDWARESET_EXT_SET_H
