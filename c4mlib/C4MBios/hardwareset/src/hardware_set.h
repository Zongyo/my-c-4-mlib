/**
 * @file hardware_set.h
 * @author ya058764
 * @date 2021.04.26
 * @brief
 *
 */
#ifndef C4MLIB_HARDWARESET_HARDWARE_SET_H
#define C4MLIB_HARDWARESET_HARDWARE_SET_H
#include <stdint.h>
/* Public Section Start */
/**
 * @brief flag put 初始化集中管理結構
 *
 * @param Reg_p 愈初始化之目標暫存器之記憶體位址 Register Pointer
 * @param Mask   遮罩。依照愈初始化暫存器中目標旗標填寫
 * @param Shift  向左位移。依照愈初始化暫存器中目標旗標填寫
 */
typedef struct {
    volatile unsigned char *Reg_p;
    unsigned char Mask;
    unsigned char Shift;
} HWFlagPara_t;

/**
 * @brief put 初始化集中管理結構
 *
 * @param Reg_p 愈初始化之目標暫存器之記憶體位址 Register Pointer
 * @param Bytes 愈傳輸資料長度。依照目標暫存器長度填寫
 */
typedef struct {
    volatile unsigned char *Reg_p;
    unsigned char Bytes;
} HWRegPara_t;

/**
 * @brief HardWare_init函式專用結構
 *
 * @param FgGpNum
 * @param RegNum
 * @param FgGpPara_p 愈初始化之目標暫存器之記憶體位址 Register Pointer
 * @param RegPara_p
 * @param List_p
 */
typedef struct {
    unsigned char FlagNum;
    unsigned char RegNum;
    HWFlagPara_t *FlagPara_p;
    HWRegPara_t *RegPara_p;
    void *DataList_p;
} HardWareSet_t;

/**
 * @brief 硬體初始化函式
 *
 * @param void_p HardWareSet結構之指標
 */
uint8_t HardwareSet_step(void *void_p);

/**
 * @brief
 * 以硬體設定參數結構體指標為傳參，被呼叫時能夠自動依據結構體內容完成硬體設定暫存器及旗標群內容的收集。
 *
 * @param void_p HardWareSet結構之指標
 */
uint8_t HardwareInsp_step(void *void_p);

#define HARDWARESET_LAY(SETSTR, FLAGPARA, REGPARA, DATALIST)                   \
    {                                                                          \
        SETSTR.FlagPara_p = &FLAGPARA;                                         \
        SETSTR.RegPara_p  = &REGPARA;                                          \
        SETSTR.FlagPara_p = &FLAGPARA;                                         \
        SETSTR.DataList_p = &DATALIST;                                         \
    }
/* Public Section End */
#endif  // C4MLIB_HARDWARESET_HARDWARE_SET_H
