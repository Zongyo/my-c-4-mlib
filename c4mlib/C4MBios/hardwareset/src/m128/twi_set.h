/**
 * @file twi_set.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.13
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARESET_M128_TWI_SET_H
#define C4MLIB_HARDWARESET_M128_TWI_SET_H

#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_def.h"

#include <stdint.h>
/* Public Section Start */

/**
 * @brief 提供twi初始化所需設定結構
 *
 */
typedef struct {
    uint8_t DDx0_1;
    uint8_t TWPSn0_1;
    uint8_t TWIEn;
    uint8_t TWENn;
    uint8_t TWAn1_7;
    uint8_t BCEn;
    uint8_t FlagTotalBytes;
    uint8_t TWIBRn;
    uint8_t RegTotalBytes;
} TWIHWSetDataStr_t;

/**
 * @brief
 * @param DataReg_p Data Register Address
 * @param CtrReg_p Control Register Address
 * @param CtrMask
 * @param CtrShift TWI Control Mask & Shift
 * @param SttReg_p State Register Address
 * @param SttMask
 * @param SttShift TWI State Mask & Shift
 * @param IntEnReg_p Int Enable Register Address
 * @param IntEnMask
 * @param IntEnShift Enable TWIint Mask & Shift
 * @param TWIEnReg_p TWI Enable Register Address
 * @param TWIEnMask
 * @param TWIEnShift Enable TWI Mask & Shift
 * @param Start
 * @param Stop start and stop Control Command
 * @param SRInf send or receive information
 * @param BCData Broad Case Data as the master MCU
 * @param SLData Send last byte of Data as a slave MCU
 */
typedef struct {
    volatile uint8_t* DataReg_p;
    volatile uint8_t* CtrReg_p;
    uint8_t CtrMask, CtrShift;
    volatile uint8_t* SttReg_p;
    uint8_t SttMask, SttShift;
    volatile uint8_t* IntEnReg_p;
    uint8_t IntEnMask, IntEnShift;
    volatile uint8_t* TWIEnReg_p;
    uint8_t TWIEnMask, TWIEnShift;
    uint8_t Start, Stop;
    uint8_t SRInf;
    uint8_t BCData;
    uint8_t SLData;
} TWIOpStr_t;

#define TWIFLAGPARALISTINI                                                     \
    {                                                                          \
        {.Reg_p = &DDRD, .Mask = 0x03, .Shift = 0},                            \
        {.Reg_p = &TWSR, .Mask = 0x03, .Shift = 0},                            \
        {.Reg_p = &TWCR, .Mask = 0x01, .Shift = 0},                            \
        {.Reg_p = &TWCR, .Mask = 0x04, .Shift = 2},                            \
        {.Reg_p = &TWAR, .Mask = 0xFE, .Shift = 1},                            \
        {.Reg_p = &TWAR, .Mask = 0x01, .Shift = 0}                             \
    }

#define TWIREGPARALISTINI                                                      \
    { .Reg_p = &TWBR, .Bytes = 1 }

#define TWIHWSETSTRINI                                                         \
    { .FlagNum = 6, .RegNum = 1 }

#define TWIOPSTRINI                                                            \
    {                                                                          \
        .DataReg_p = &TWDR, .CtrReg_p = &TWCR, .CtrMask = 0xF0, .CtrShift = 0, \
        .SttReg_p = &TWSR, .SttMask = 0xF8, .SttShift = 0,                     \
        .IntEnReg_p = &TWCR, .IntEnMask = 0X04, .IntEnShift = 2,               \
        .TWIEnReg_p = &TWCR, .TWIEnMask = 0X01, .TWIEnShift = 0,               \
        .Start = 0xA0, .Stop = 0X90, .SRInf = 0XC0, .BCData = 0X80,            \
        .SLData = 0x80                                                         \
    }

/* Public Section End */
#endif  // C4MLIB_HARDWARESET_M128_TWI_SET_H
