/**
 * @file tim_set.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.13
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARESET_M128_TIM_SET_H
#define C4MLIB_HARDWARESET_M128_TIM_SET_H

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/macro/src/std_def.h"


/* Public Section Start */

/**
 * @brief
 * @param IntEnReg_p Interrupt Enable flag Reg address
 * @param IntEnMask Interrupt Enable flag Mask, shift
 * @param IntEnShift
 * @param IntSetReg_p Interrupt flag Set Reg address
 * @param IntSetMask  Interrupt flag Set Mask, shift
 * @param IntSetShift
 * @param CountReg_p T/C Counter Register addr
 * @param CountBytes T/C Counter bytes
 * @param PeriodReg_p T/C Counter Period set Register addr
 * @param PeriodBytes T/C Counter Period set bytes
 */
typedef struct {
    volatile uint8_t* IntEnReg_p;
    uint8_t IntEnMask;
    uint8_t IntEnShift;
    volatile uint8_t* IntSetReg_p;
    uint8_t IntSetMask;
    uint8_t IntSetShift;
    volatile uint8_t* CountReg_p;
    uint8_t CountBytes;
    volatile uint8_t* PeriodReg_p;
    uint8_t PeriodBytes;
} TIMOpStr_t;

/**
 * @brief timer0
 *
 * @param ASn ClockSource
 * @param WGMn0_1 WaveSelection
 * @param CSn0_2 TIME0FreqDivide
 * @param COMn0_1 TIME0WareOut
 * @param DDx WaveOutPin
 * @param FlagTotalBytes Total Bytes of Flag Group Datum
 * @param OCRn Adjust TIMER0 Cycle
 * @param RegTotalBytes Total Bytes of Registers
 */
typedef struct {
    uint8_t ASn;
    uint8_t WGMn0_1;
    uint8_t CSn0_2;
    uint8_t COMn0_1;
    uint8_t DDx;
    uint8_t TIM0EN;
    uint8_t FlagTotalBytes;
    uint8_t OCRn;
    uint8_t RegTotalBytes;
} TIM0HWSetDataStr_t;

/**
 * @brief timer1 timer3
 *
 * @param WGMn0_1 WaveSelection
 * @param WGMn2_3 WaveSelection
 * @param CSn0_2 TIME1FreqDivide
 * @param COMnA0_1 TIME1WareOut
 * @param DDx WaveOutPin
 * @param FlagTotalBytes Total Bytes of Flag Group Datum
 * @param OCRn Adjust TIMER1 Cycle
 * @param RegTotalBytes Total Bytes of Registers
 */
typedef struct {
    uint8_t WGMn0_1;       
    uint8_t WGMn2_3;       
    uint8_t CSn0_2;        
    uint8_t COMnA0_1;      
    uint8_t DDx;           
    uint8_t TIM1EN;
    uint8_t FlagTotalBytes;
    uint16_t OCRn;         
    uint8_t RegTotalBytes; 
} TIM1HWSetDataStr_t;

typedef struct {
    uint8_t WGMn0_1;       
    uint8_t WGMn2_3;       
    uint8_t CSn0_2;        
    uint8_t COMnA0_1;      
    uint8_t DDx;           
    uint8_t TIM3EN;
    uint8_t FlagTotalBytes;
    uint16_t OCRn;         
    uint8_t RegTotalBytes; 
} TIM3HWSetDataStr_t;

/**
 * @brief timer 2
 *
 * @param WGMn0_1 WaveSelection
 * @param CSn0_2 TIME0FreqDivide
 * @param COMn0_1 TIME0WareOut
 * @param DDx WaveOutPin
 * @param FlagTotalBytes Total Bytes of Flag Group Datum
 * @param OCRn Adjust TIMER0 Cycle
 * @param RegTotalBytes Total Bytes of Registers
 */
typedef struct {
    uint8_t WGMn0_1;
    uint8_t CSn0_2;
    uint8_t COMn0_1;
    uint8_t DDx;
    uint8_t TIM2EN;
    uint8_t FlagTotalBytes;
    uint8_t OCRn;
    uint8_t RegTotalBytes;
} TIM2HWSetDataStr_t;

#define TIM0FLAGPARALISTINI                                                    \
    {                                                                          \
        {.Reg_p = &ASSR, .Mask = 0x08, .Shift = 3},                            \
        {.Reg_p = &TCCR0, .Mask = 0x48, .Shift = 3},                           \
        {.Reg_p = &TCCR0, .Mask = 0x07, .Shift = 0},                           \
        {.Reg_p = &TCCR0, .Mask = 0x30, .Shift = 4},                           \
        {.Reg_p = &DDRB, .Mask = 0x10, .Shift = 4},                            \
        {.Reg_p = &TIMSK, .Mask = 0x02, .Shift = 1}                            \
    }

#define TIM1FLAGPARALISTINI                                                    \
    {                                                                          \
        {.Reg_p = &TCCR1A, .Mask = 0x03, .Shift = 0},                          \
        {.Reg_p = &TCCR1B, .Mask = 0x18, .Shift = 3},                          \
        {.Reg_p = &TCCR1B, .Mask = 0x07, .Shift = 0},                          \
        {.Reg_p = &TCCR1A, .Mask = 0xc0, .Shift = 6},                          \
        {.Reg_p = &DDRB, .Mask = 0x20, .Shift = 5},                            \
        {.Reg_p = &TIMSK, .Mask = 0x10, .Shift = 4}                            \
    }

#define TIM2FLAGPARALISTINI                                                    \
    {                                                                          \
        {.Reg_p = &TCCR2, .Mask = 0x48, .Shift = 3},                           \
        {.Reg_p = &TCCR2, .Mask = 0x07, .Shift = 0},                           \
        {.Reg_p = &TCCR2, .Mask = 0x30, .Shift = 4},                           \
        {.Reg_p = &DDRB, .Mask = 0x80, .Shift = 7},                            \
        {.Reg_p = &TIMSK, .Mask = 0x80, .Shift = 7}                            \
    }

#define TIM3FLAGPARALISTINI                                                    \
    {                                                                          \
        {.Reg_p = &TCCR3A, .Mask = 0x03, .Shift = 0},                          \
        {.Reg_p = &TCCR3B, .Mask = 0x18, .Shift = 3},                          \
        {.Reg_p = &TCCR3B, .Mask = 0x07, .Shift = 0},                          \
        {.Reg_p = &TCCR3A, .Mask = 0xC0, .Shift = 6},                          \
        {.Reg_p = &DDRE, .Mask = 0x08, .Shift = 3},                            \
        {.Reg_p = &ETIMSK, .Mask = 0x10, .Shift = 4}                           \
    }

#define TIM0REGPARALISTINI                                                     \
    { .Reg_p = &OCR0, .Bytes = 1 }

#define TIM1REGPARALISTINI                                                     \
    { .Reg_p = &OCR1AL, .Bytes = 2 }

#define TIM2REGPARALISTINI                                                     \
    { .Reg_p = &OCR2, .Bytes = 1 }

#define TIM3REGPARALISTINI                                                     \
    { .Reg_p = &OCR3AL, .Bytes = 2 }

#define TIM0HWSETSTRINI                                                     \
    { .FlagNum = 6, .RegNum = 1 }

#define TIM1HWSETSTRINI                                                     \
    { .FlagNum = 6, .RegNum = 1 }

#define TIM2HWSETSTRINI                                                     \
    { .FlagNum = 5, .RegNum = 1 }

#define TIM3HWSETSTRINI                                                     \
    { .FlagNum = 6, .RegNum = 1 }


#define TIM0OPSTRINI                                                           \
    {                                                                          \
        .IntEnReg_p = &TIMSK, .IntEnMask = 0x02, .IntEnShift = 1,              \
        .IntSetReg_p = &TIFA, .IntSetMask = 0x02, .IntSetShift = 1,            \
        .CountReg_p = &TCNT0, .CountBytes = 1, .PeriodReg_p = &OCR0,           \
        .PeriodBytes = 1                                                       \
    }

#define TIM1OPSTRINI                                                           \
    {                                                                          \
        .IntEnReg_p = &TIMSK, .IntEnMask = 0x10, .IntEnShift = 4,              \
        .IntSetReg_p = &TIFR, .IntSetMask = 0x10, .IntSetShift = 4,            \
        .CountReg_p = &TCNT1L, .CountBytes = 2, .PeriodReg_p = &OCR1AL,        \
        .PeriodBytes = 2                                                       \
    }

#define TIM2OPSTRINI                                                           \
    {                                                                          \
        .IntEnReg_p = &TIMSK, .IntEnMask = 0x80, .IntEnShift = 7,              \
        .IntSetReg_p = &TIFR, .IntSetMask = 0x80, .IntSetShift = 7,            \
        .CountReg_p = &TCNT2, .CountBytes = 1, .PeriodReg_p = &OCR2,           \
        .PeriodBytes = 1                                                       \
    }

#define TIM3OPSTRINI                                                           \
    {                                                                          \
        .IntEnReg_p = &ETIMSK, .IntEnMask = 0x10, .IntEnShift = 4,             \
        .IntSetReg_p = &ETIFR, .IntSetMask = 0x10, .IntSetShift = 4,           \
        .CountReg_p = &TCNT3L, .CountBytes = 2, .PeriodReg_p = &OCR3AL,        \
        .PeriodBytes = 2                                                       \
    }

/* Public Section End */

#endif  // C4MLIB_HARDWARESET_M128_TIM_SET_H
