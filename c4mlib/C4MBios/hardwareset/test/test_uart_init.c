/**
 * @file test_uart_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.15
 * @brief 
 * 
 */

#include "c4mlib/C4MBios/hardwareset/src/m128/uart_set.h"
#include "c4mlib/config/uart.cfg"
#include "c4mlib/C4MBios/device/src/device.h"

uint8_t UART1_init(void){
    HardWareSet_t UART1HWSet_str = UART1HWSETSTRINI;
    HWFlagPara_t UART1FgGpData_str[9] = UART1FLAGPARALISTINI;
    HWRegPara_t UART1RegData_str[2] = UART1REGPARALISTINI;
    UART1HWSetDataStr_t UART1HWSetData = UART1SETDATALISTINI;
    HARDWARESET_LAY(UART1HWSet_str, UART1FgGpData_str[0], UART1RegData_str[0], UART1HWSetData);
    return HardwareSet_step(&UART1HWSet_str);
}


int main() {
    C4M_DEVICE_set();
    uint8_t a = UART1_init();
    printf("hardware_set report %d \n", a);
    printf("UMP10_1 = %d\n", ((UCSR1C & 0x30) >> 4));
    printf("USBS1 = %d\n", ((UCSR1C & 0x08) >> 3));
    printf("UCSZ10_1 = %d\n", ((UCSR1C & 0x06) >> 1));
    printf("TXEN1 = %d\n", ((UCSR1B & 0x08) >> 3));
    printf("RXEN1 = %d\n", ((UCSR1B & 0x10) >> 4));
    printf("TXCIE1 = %d\n", ((UCSR1B & 0x40) >> 6));
    printf("RXCIE1 = %d\n", ((UCSR1B & 0x80) >> 7));
    printf("U2X1 = %d\n", ((UCSR1A & 0x02) >> 1));
    printf("DDRE = %d\n", (DDRE & 0x03));
    printf("UBRR1 = %d\n", UBRR1L + (UBRR1H << 8));
}
