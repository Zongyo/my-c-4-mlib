/**
 * @file test_ext5_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.14
 * @brief 
 * 
 */
#include "c4mlib/C4MBios/hardwareset/src/m128/ext_set.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/config/ext.cfg"

#include <avr/interrupt.h>
int i=0;


uint8_t EXT5_init(void){
    HardWareSet_t EXT5HWSet_str = EXTHWSETSTRINI;
    HWFlagPara_t EXT5FgGpData_str[3] = EXT5FLAGPARALISTINI;
    HWRegPara_t EXT5RegData_str = EXT5REGPARALISTINI;
    EXTHWsetDataStr_t EXT5HWSetData = EXT5SETDATALISTINI;
    HARDWARESET_LAY(EXT5HWSet_str, EXT5FgGpData_str[0], EXT5RegData_str, EXT5HWSetData);
    return HardwareSet_step(&EXT5HWSet_str);
}
int main(){
    C4M_DEVICE_set();
    uint8_t res = EXT5_init();
    printf("hardware_set report %d \n", res);
    printf("DDRE = %d\n", ((DDRE & 0x20) >> 5));
    printf("EICRB = %d\n", ((EICRB & 0x0c) >> 2));
    printf("EIMSK = %d\n", ((EIMSK & 0x20) >> 5));
    sei();
    while (1);
}

ISR(INT5_vect)
{
   i++;
   printf("ext complete, i=%d\n", i);
}
