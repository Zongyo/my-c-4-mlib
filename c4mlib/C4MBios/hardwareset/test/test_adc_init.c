/**
 * @file test_adc_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.14
 * @brief 
 * 
 */
#include "c4mlib/C4MBios/hardwareset/src/m128/adc_set.h"
#include "c4mlib/config/adc.cfg"
#include "c4mlib/C4MBios/device/src/device.h"

uint8_t ADC_init(void){
    HardWareSet_t ADCHWSet_str = ADCHWSETSTRINI;
    HWFlagPara_t ADCFgGpData_str[8] = ADCFLAGPARALISTINI;
    HWRegPara_t ADCRegData_str = ADCREGPARALISTINI;
    ADCHWSetDataStr_t ADCHWSetData = ADCSETDATALISTINI;
    HARDWARESET_LAY(ADCHWSet_str, ADCFgGpData_str[0], ADCRegData_str, ADCHWSetData);
    return HardwareSet_step(&ADCHWSet_str);
}


int main(){
    C4M_DEVICE_set();
    
    uint8_t res = ADC_init();
    printf("hardware_set report %d \n", res);
    printf("DDx0_7   = %d\n", ((DDRF & 0xFF) >> 0));
    printf("MUXn0_4  = %d\n", ((ADMUX & 0x1F) >> 0));
    printf("REFSn0_4 = %d\n", ((ADMUX & 0xC0) >> 6));
    printf("ADCLARn  = %d\n", ((ADMUX & 0x20) >> 5));
    printf("ADPSn0_2 = %d\n", ((ADCSRA & 0x07) >> 0));
    printf("ADFRn    = %d\n", ((ADCSRA & 0x20) >> 5));
    printf("ADENn    = %d\n", ((ADCSRA & 0x80) >> 7));
    printf("ADIEn    = %d\n", ((ADCSRA & 0x08) >> 3));

    return 0;
}
