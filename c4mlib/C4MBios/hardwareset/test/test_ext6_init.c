/**
 * @file test_ext6_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.14
 * @brief 
 * 
 */
#include "c4mlib/C4MBios/hardwareset/src/m128/ext_set.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/config/ext.cfg"
#include <avr/interrupt.h>
int i=0;


uint8_t EXT6_init(void){
    HardWareSet_t EXT6HWSet_str = EXTHWSETSTRINI;
    HWFlagPara_t EXT6FgGpData_str[3] = EXT6FLAGPARALISTINI;
    HWRegPara_t EXT6RegData_str = EXT6REGPARALISTINI;
    EXTHWsetDataStr_t EXT6HWSetData = EXT6SETDATALISTINI;
    HARDWARESET_LAY(EXT6HWSet_str, EXT6FgGpData_str[0], EXT6RegData_str, EXT6HWSetData);
    return HardwareSet_step(&EXT6HWSet_str);
}
int main(){
    C4M_DEVICE_set();
    uint8_t res = EXT6_init();
    printf("hardware_set report %d \n", res);
    printf("DDRE = %d\n", ((DDRE & 0x40) >> 6));
    printf("EICRB = %d\n", ((EICRB & 0x30) >>4));
    printf("EIMSK = %d\n", ((EIMSK & 0x40) >> 6));
    sei();
    while (1);
}

ISR(INT6_vect)
{
   i++;
    printf("ext complete, i=%d\n", i);
}
