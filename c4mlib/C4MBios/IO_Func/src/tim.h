/**
 * @file tim.h
 * @author LiYu87
 * @date 2019.07.29
 * @author ya058764
 * @date 2021.06.05
 * @brief 宣告 timer 暫存器操作函式原型。
 */

#ifndef C4MLIB_IOFUNC_TIM_H
#define C4MLIB_IOFUNC_TIM_H
#include <stdint.h>
/**
 * @defgroup hw_tim_func hardware timer functions
 */

/* Public Section Start */
/**
 * @brief tim flag put 函式
 *
 * @ingroup hw_tim_func
 * @param REG_p 暫存器實際位址。
 * @param Mask   遮罩。
 * @param Shift  向左位移。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 LSByte 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
uint8_t TIM_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data);

/**
 * @brief tim flag get 函式
 *
 * @ingroup hw_tim_func
 * @param REG_p 暫存器實際位址。
 * @param Mask   遮罩。
 * @param Shift  向右位移。
 * @param Data_p 資料指標。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 LSByte 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
uint8_t TIM_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, void *Data_p);

/**
 * @brief tim put 函式
 *
 * @ingroup hw_tim_func
 * @param REG_p 暫存器實際位址。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 */
uint8_t TIM_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);

/**
 * @brief tim get 函式
 *
 * @ingroup hw_tim_func
 * @param REG_p 暫存器實際位址。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 */
uint8_t TIM_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);

/**
 * @brief 與 TIM_fpt 相同。
 */
uint8_t PWM_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data);

/**
 * @brief 與 TIM_fgt 相同。
 */
uint8_t PWM_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, void *Data_p);

/**
 * @brief 與 TIM_put 相同。
 */
uint8_t PWM_put(volatile unsigned char *REG_p, uint8_t Bytes, void* Data_p);

/**
 * @brief 與 TIM_get 相同。
 */
uint8_t PWM_get(volatile unsigned char *REG_p, uint8_t Bytes, void* Data_p);
/* Public Section End */

#endif  // C4MLIB_IOFUNC_TIM_H
