/**
 * @file adc.c
 * @author s915888
 * @date 2020.05.11
 * @author ya058764
 * @date 2021.06.05
 * @brief adc 暫存器操作相關函式實作。
 *
 */
#include "c4mlib/C4MBios/IO_Func/src/adc.h"

#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"

#include <avr/io.h>

/* public functions declaration section ------------------------------------- */
uint8_t ADC_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data);
uint8_t ADC_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
             void *Data_p);
uint8_t ADC_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);
uint8_t ADC_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);
uint8_t ADC_set(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data)
    __attribute__((alias("ADC_fpt")));

/* public function contents section ----------------------------------------- */
uint8_t ADC_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data) {
    if (Shift <= 7) {
        if (REG_p != &ADMUX && REG_p != &ADCSRA && REG_p != &DDRF) {
            return RES_ERROR_LSBYTE;
        }
        else {
            REGFPT(REG_p, Mask, Shift, Data);
        }
    }
    else {
        return RES_ERROR_SHIFT;
    }
    return RES_OK;
}

uint8_t ADC_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
             void *Data_p) {
    if (Shift <= 7) {
        if (REG_p != &ADCSRA) {
            return RES_ERROR_LSBYTE;
        }
        else {
            REGFGT(REG_p, Mask, Shift, Data_p);
        }
    }
    else {
        return RES_ERROR_SHIFT;
    }
    return RES_OK;
}

uint8_t ADC_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    return RES_OK;
}

uint8_t ADC_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (REG_p == &ADCH) {
        if (Bytes == 1) {
            *((char *)Data_p) = ADCH;
        }
        else {
            return RES_ERROR_BYTES;
        }
    }
    else if (REG_p == &ADCL) {
        if (Bytes == 2) {
            *((char *)Data_p) = ADCL;
            *((char *)Data_p + 1) = ADCH;
        }
        else {
            return RES_ERROR_BYTES;
        }
    }
    else {
        return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}
