/**
 * @file tim.c
 * @author LiYu87
 * @date 2019.04.02
 * @author ya058764
 * @date 2021.06.05
 * @brief tim 暫存器操作相關函式實作。
 */

#include "c4mlib/C4MBios/IO_Func/src/tim.h"

#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"

#include <avr/io.h>

uint8_t TIM_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                uint8_t Data) {
    if (Shift > 7) {
        return RES_ERROR_SHIFT;
    }
    if (REG_p != &DDRA && REG_p != &DDRB && REG_p != &DDRC && REG_p != &DDRD &&
        REG_p != &DDRE && REG_p != &DDRF && REG_p != &TIMSK && REG_p != &TIFR &&
        REG_p != &ETIMSK && REG_p != &ETIFR && REG_p != &SFIOR &&
        REG_p != &ASSR && REG_p != &TCCR0 && REG_p != &TCCR1A &&
        REG_p != &TCCR1B && REG_p != &TCCR1C && REG_p != &ACSR &&
        REG_p != &TCCR2 && REG_p != &TCCR3A && REG_p != &TCCR3B &&
        REG_p != &TCCR3C) {
        return RES_ERROR_LSBYTE;
    }
    REGFPT(REG_p, Mask, Shift, Data);
    return RES_OK;
}

uint8_t TIM_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                void *Data_p) {
    if (Shift > 7) {
        return RES_ERROR_SHIFT;
    }
    if (REG_p != &TIFR && REG_p != &ETIFR) {
        return RES_ERROR_LSBYTE;
    }
    REGFGT(REG_p, Mask, Shift, Data_p);
    return RES_OK;
}

uint8_t TIM_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (Bytes != 1 && Bytes != 2) {
        return RES_ERROR_BYTES;
    }
    if (REG_p == &TCNT0 || REG_p == &TCNT2) {
        if (Bytes != 1) {
            return RES_ERROR_BYTES;
        }
        *REG_p = *((volatile unsigned char *)Data_p);
        return RES_OK;
    }
    else if (REG_p == &ICR1L || REG_p == &ICR3L) {
        for (char i = 0; i < Bytes; i++) {
            *(REG_p + i) = *((volatile unsigned char *)(Data_p + i));
        }
        return RES_OK;
    }
    return RES_ERROR_LSBYTE;
}

uint8_t TIM_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (Bytes != 1 && Bytes != 2) {
        return RES_ERROR_BYTES;
    }
    if (REG_p == &OCR0 || REG_p == &TCNT0 || REG_p == &OCR2 ||
        REG_p == &TCNT2) {
        if (Bytes != 1) {
            return RES_ERROR_BYTES;
        }
        *REG_p = *((volatile unsigned char *)(Data_p));
        return RES_OK;
    }
    else if (REG_p == &ICR1L || REG_p == &OCR1AL || REG_p == &OCR1BL ||
             REG_p == &OCR1CL || REG_p == &TCNT1L || REG_p == &ICR3L ||
             REG_p == &OCR3AL || REG_p == &OCR3BL || REG_p == &OCR3CL ||
             REG_p == &TCNT3L) {
        for (char i = 0; i < Bytes; i++) {
            *(REG_p + i) = *((volatile unsigned char *)(Data_p + i));
        }
        return RES_OK;
    }
    return RES_ERROR_LSBYTE;
}

uint8_t PWM_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data)
    __attribute__((alias("TIM_fpt")));

uint8_t PWM_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, void *Data_p)
    __attribute__((alias("TIM_fgt")));

uint8_t PWM_put(volatile unsigned char *REG_p, uint8_t Bytes, void* Data_p)
    __attribute__((alias("TIM_put")));

uint8_t PWM_get(volatile unsigned char *REG_p, uint8_t Bytes, void* Data_p)
    __attribute__((alias("TIM_put")));
