/**
 * @file hardware.h
 * @author LiYu87
 * @date 2019.10.8
 * @brief 引用所有 hardware 標頭檔，方便引用
 */

#ifndef C4MLIB_IOFUNC_IOFUNC_H
#define C4MLIB_IOFUNC_IOFUNC_H

/**
 * @defgroup hw_func hardware functions
 * @defgroup hw_hal_struct hardware hal structs
 * @defgroup hw_hal_func hardware hal functions
 */

#include "adc.h"
#include "dio.h"
#include "spi.h"
#include "tim.h"
#include "twi.h"
#include "uart.h"


#endif  // C4MLIB_IOFUNC_IOFUNC_H
