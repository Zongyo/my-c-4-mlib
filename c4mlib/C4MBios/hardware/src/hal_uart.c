/**
 * @file hal_uart.c
 * @author Ye cheng-Wei
 * @date 2019.1.22
 * @brief uart 硬體抽象層 各硬體實作分割。
 */

#if defined(__AVR_ATmega128__)
#    include "m128/hal_uart.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/hal_uart.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/hal_uart.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
