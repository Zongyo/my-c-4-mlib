/**
 * @file test_REG_macro.c
 * @author ya058764
 * @date 2020.03.25
 * @brief 測試REGPUT、REGGET macro運作
 */

#include "c4mlib/C4MBios/device/src/device.h"

#include <avr/io.h>

int main() {
    C4M_DEVICE_set();
    unsigned char send_data[2], rec_data[2];
    uint16_t data = 306, rec;
    send_data[0] = LOBYTE16(data);
    send_data[1] = HIBYTE16(data);
    REGPUT(&OCR1AL, 2, send_data);
    REGGET(&OCR1AL, 2, rec_data);
    rec = (rec_data[0] | (uint16_t)(rec_data[1] << 8));
    printf("%d,%d", OCR1A, rec);
}
