/**
 * @file rtupcount.c
 * @author Yi-Mou
 * @brief reatimeupcount實現
 * @date 2019-08-26
 */

#include "rtupcount.h"
#include <stddef.h>


void RealTimeISRCount_step(void* VoidStr_p){
    RealTimeISRCountStr_t* Str_p =(RealTimeISRCountStr_t*)VoidStr_p;
    Str_p->ExCount++;
}
