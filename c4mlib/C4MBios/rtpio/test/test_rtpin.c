/**
 * @file test_rtpin.c
 * @author Yi-Mou
 * @brief rtpio實作
 * @date 2019-08-19
 *
 * 不使用中斷，只測試step函式作用及函式用法
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include <avr/io.h>

#include "c4mlib/C4MBios/rtpio/src/rtpio.h"



int main(){

    C4M_DEVICE_set();
    uint8_t data = 0;
    RT_REG_IO_LAY(RealTimePortIn_1,0,&PINA,1,&data);
    while(1){
        RealTimeRegGet_step(&RealTimePortIn_1);
        
        printf("data=%d\n", data);
    }
}
