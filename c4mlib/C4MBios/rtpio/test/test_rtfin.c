/**
 * @file test_rtfin.c
 * @author Yi-Mou
 * @brief rtfio實作
 * @date 2019-09-13
 *
 * 不使用中斷，只測試step函式作用及函式用法。
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/rtpio/src/rtpio.h"

#include <avr/io.h>

int main() {
    C4M_DEVICE_set();

    uint8_t data = 0;
    RT_FLAG_IO_LAY(RealTimeFlagIn_1, 1, &PINA, 0xff, 0, &data);

    while (1) {
        RealTimeFlagGet_step(&RealTimeFlagIn_1);
        if (RealTimeFlagIn_1.ExCount == 1) {
            data                       = *RealTimeFlagIn_1.uIn_p;
            RealTimeFlagIn_1.ExCount = 0;
        }
        printf("data=%d\n", data);
    }
}
