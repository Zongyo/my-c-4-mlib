/**
 * @file test_put_matrix.c
 * @author LiYu87
 * @brief 測試函式 HMI_put_matrix
 * @date 2019.08.21
 *
 * 測試 HMI_put_matrix 能否正確將資料送出。
 * 建議配合人機進行測試，或使用py serial接收原始byte來檢查。
 */

#include "c4mlib/SysOperat/asahmi/src/asa_hmi.h"
#include "c4mlib/C4MBios/device/src/device.h"

int main() {
    C4M_STDIO_init();

    uint8_t data[2][5] = {
        {0, 1, 2, 3, 4},
        {5, 6, 7, 8, 9}
    };

    HMI_put_matrix(HMI_TYPE_UI8, 2, 5, data);

    return 0;
}
