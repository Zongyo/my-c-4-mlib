/**
 * @file w25q128jv.h
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2020.10.22
 * @brief 
 *      This is the hardware definition for 128mb flash.
 *      
 * 
 */

#ifndef W25Q128JV_H
#define W25Q128JV_H

// memory size info
#define PAGE_SIZE       256
#define SECTOR_SIZE     ((uint32_t)PAGE_SIZE * 16)
#define BLOCK_SIZE      ((uint32_t)SECTOR_SIZE * 16)
#define REWRITE_SHIFT   (BLOCK_SIZE)    

// Start point of block (0 ~ 255)
// BLOCK_0:   0x000000 ~ 0x00FFFF
// BLOCK_255: 0xFF0000 ~ 0xFFFFFF     
#define BLOCK(n)        (0x000000 + (n * BLOCK_SIZE))

// Select sector in block
// *Block: block macro
// *Sector_Index: sector in block (0 ~ 15)
#define SECTOR(Block,Sector_Index) (Block + ((uint32_t)Sector_Index * SECTOR_SIZE))

#define NOT_USE         0
#define UNUSED_ID       0x99
// #define SPI_DUMMY       0x99

// System distribution
#define DISC_STR_SPACE      SECTOR_SIZE
#define DISC_INFO           BLOCK(0)
#define FILE_INFO           (BLOCK(0) + DISC_STR_SPACE)
#define DATA_START          BLOCK(1)    
#define DATA_END            BLOCK(250)    
// #define DATA_SECTOR_LINK_BLOCK   BLOCK_1
// #define DISC_INFO_2              BLOCK_2
// #define FILE_INFO_2              (BLOCK_2 + DISC_STR_SPACE)
// #define DATA_SECTOR_LINK_BLOCK_2 BLOCK_3

// w25q128jv commands
#define WRITE_ENABLE    0x06
#define WRITE_DISABLE   0x04
#define READ_DATA       0x03
#define READ_MANU       0x90
#define PAGE_PROGRAM    0x02
#define ENTIRE_ERASE    0xC7
#define SECTOR_ERASE    0x20
#define BLOCK_ERASE     0xD8    // 64kb (Entire block)
#define READ_STATUS     0x05
#define ADDR_BYTES      3

#endif  // W25Q128JV_H