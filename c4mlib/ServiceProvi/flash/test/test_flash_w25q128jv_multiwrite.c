/**
 * @file test_flash_w25q128jv_write.c
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2020.10.07
 * @brief 使用SPI Master記憶傳輸和接收驅動函式測試w25q128jv flash IC
 * 蓋寫資料功能。
 *
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#define SPI_MODE        6
#define ASAID           8

uint32_t mem_addr = 0;
uint8_t manu_cont[3] = {0, 0, 0};
uint8_t test_rec[4] = {0, 0, 0 ,0};
uint8_t test_id[2] = {0, 0};
uint8_t test_write[3] = {0xFF, 0x11, 0x11};
uint8_t addr[3] = {0, 0, 0};

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main() {
    // Setup
    uint64_t ADDR = 0x00;

    C4M_STDIO_init();
    // FIXME: 待更新..
    DDRB |= 0x07;
    SPI_init();

    Extern_CS_disable(ASAID);
    printf("Start test flash Read & Write...\n");
    while(true) {
        printf("=================\n");

        Extern_CS_enable(ASAID);
        ASABUS_SPI_swap(SPI_DUMMY);  
        Extern_CS_disable(ASAID);

        printf("--WRITE_ENABLE--\n");
        SPIM_Mem_trm(SPI_MODE, ASAID, WRITE_ENABLE, 0, &mem_addr, 0,
                     manu_cont);
        Flash_wait_to_complete();

        printf("--PAGE_PROGRAM--\n");
        SPIM_Mem_trm(SPI_MODE, ASAID, PAGE_PROGRAM, 3, &ADDR, 1,
                     test_write);
        Flash_wait_to_complete();

        // // Next page write test
        printf("--WRITE_ENABLE--\n");
        SPIM_Mem_trm(SPI_MODE, ASAID, WRITE_ENABLE, 0, 0, 0,0);
        Flash_wait_to_complete();
        printf("--PAGE_PROGRAM--2--\n");
        test_write[0] = 0x66;
        // ADDR = ADDR + (sizeof(uint8_t) * 1);
        SPIM_Mem_trm(SPI_MODE, ASAID, PAGE_PROGRAM, 3, &ADDR, 1,
                     test_write);
        Flash_wait_to_complete();


        // // Next page write test
        printf("--WRITE_ENABLE--\n");
        SPIM_Mem_trm(SPI_MODE, ASAID, WRITE_ENABLE, 0, &mem_addr, 0,
                     manu_cont);
        Flash_wait_to_complete();
        printf("--PAGE_PROGRAM--2--\n");
        test_write[0] = 0x77;
        ADDR = ADDR + (sizeof(uint8_t) * 1);
        SPIM_Mem_trm(SPI_MODE, ASAID, PAGE_PROGRAM, 3, &ADDR, 1,
                     test_write);
        Flash_wait_to_complete();

        printf("--READ_4BYTES_DATA--\n");
        ADDR = 0x00;
        SPIM_Mem_rec(SPI_MODE, ASAID, READ_DATA, 3, &ADDR, 4, test_rec);
        Flash_wait_to_complete();
        
        printf("read = %d,%d,%d\n", test_rec[0], test_rec[1], test_rec[2]);

        break;
        _delay_ms(2000);
    }

}
