/**
 * @file test_flash_delet.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.04.01
 * @brief 測試FileDelete()運作
 * 測試flash_delete中，以name來刪除檔案的功能
 * 步驟:
 *     用ID open file
 *     檢查file open
 * return值，如為FLASH_SUCCESS_OPEN_FILE_BYNAME或FLASH_NEW_FILE_SUCCESS_BYNAME即可繼續
 *     接著利用name 將剛才open的檔案delete，檢查回傳值及disc_info中的file_total
 *     最後再用同樣name open
 * file，檢查return值是否為FLASH_NEW_FILE_SUCCESS_BYNAME
 *
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#define SPI_MODE  5
#define ASAID     8
#define SPI_DUMMY 0x77

DiscStr_t test_disc;
FileStr_t test_file;
uint8_t CardID       = 8;
uint8_t mem_addr[3]  = {0, 0, 0};
uint8_t manu_cont[2] = {0, 0};
uint8_t fileID       = 0;
char fileNAME[8]     = {0};

uint8_t SPI_init(void) {
    HardWareSet_t SPIHWSet_str       = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str       = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData   = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str,
                    SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void) {
    // Setup
    C4M_DEVICE_set();
    DDRB |= 0x07;
    SPI_init();
    // FIXME: 待更新..
    Extern_CS_disable(ASAID);

    fileNAME[0] = 'T';
    fileNAME[1] = 'E';
    fileNAME[2] = 's';
    fileNAME[3] = 'T';

    printf("Start test flash Manufacturer ID reading...\n");
    printf("=================\n");
    Flash_manufacture_code(manu_cont);
    for (uint8_t i = 0; i < 2; i++) {
        printf("manu_cont=>%x\n", manu_cont[i]);
    }

    // int a = FlashFormat(&test_disc, CardID);
    // printf("---Format complete!!!---, return = %d\n", a);

    int a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);
    printf("test_disc.CardId = %d\n", test_disc.CardId);
    printf("test_disc.FileMax = %d\n", test_disc.FileMax);
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);

    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    printf("---File open--, return = %d\n", a);
    a = FileClose(&test_disc, &test_file);
    printf("---File close--, return = %d\n", a);
    printf("FileName ASCII--: ");
    for (uint8_t i = 0; i < FlASH_NAME_LENGTH; i++) {
        printf("%d ,", test_file.FileName[i]);
    }
    printf("\n");
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("DStr_p -> Writen =  %d\n", test_disc.Writen);

    a = FileDelete(&test_disc, fileID, test_file.FileName);
    printf("---File delete--, return: %d\n", a);
    printf("Total file = %d\n", test_disc.FileTotal);

    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    printf("---File open--, return = %d\n", a);
    a = FileClose(&test_disc, &test_file);
    printf("---File close--, return = %d\n", a);
    printf("FileName ASCII--: ");
    for (uint8_t i = 0; i < FlASH_NAME_LENGTH; i++) {
        printf("%d ,", test_file.FileName[i]);
    }
    printf("\n");
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("DStr_p -> Writen =  %d\n", test_disc.Writen);
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("-----END-----\n");

    return 0;
}
