/**
 * @file test_flash_format.c
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2020.10.29
 * @brief 
 * 
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#define SPI_MODE        6
#define ASAID           8
#define SPI_DUMMY       0x77

uint8_t mem_addr[3] = {0, 0, 0};
uint8_t manu_cont[2] = {0, 0};

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void) {
    // Setup
    C4M_STDIO_init();
    uint32_t ADDR = 0x010000;
    uint16_t test_rec[2]={0};
    DDRB |= 0x07;
    SPI_init();
    // FIXME: 待更新..

    Extern_CS_disable(ASAID);

    DiscStr_t test_disc;
    uint8_t CardID = 8;
    
    printf("Start test flash Manufacturer ID reading...\n");
    printf("=================\n");

    while(true) {

        SPIM_Mem_rec(SPI_MODE, ASAID, READ_MANU, 3, mem_addr,
                     sizeof(manu_cont), manu_cont);
        Flash_wait_to_complete();
        for (uint8_t i = 0; i < 2; i++) {
            printf("manu_cont=>%x\n", manu_cont[i]);
        }
        FlashFormat(&test_disc, CardID);

        break;
        _delay_ms(2000);
    }
    printf("---Format complete!!!---\n");
    DiscStr_t tested_disc;
    ADDR = DISC_INFO;
    SPIM_Mem_rec(SPI_MODE, ASAID, READ_DATA, 3, &ADDR, sizeof(DiscStr_t), &tested_disc);
    printf("%x %lx\n",tested_disc.CardId,tested_disc.FSLLTail);
    ADDR = DATA_START;
    for(uint32_t i=0;i<64;i++){
        printf("--READ_4BYTES_DATA %2ld--\n",i);
        SPIM_Mem_rec(SPI_MODE, ASAID, READ_DATA, 3, &ADDR, 4, test_rec);
        Flash_wait_to_complete();
        printf("ADDR=%lx read: Sector = %x,NextLink = %x\n", ADDR, test_rec[0], test_rec[1]);
        ADDR=ADDR+SECTOR_SIZE;
        _delay_ms(1000);
    }
    return 0;
}
