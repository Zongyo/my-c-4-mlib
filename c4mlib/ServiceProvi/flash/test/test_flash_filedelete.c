/**
 * @file test_flash_filedelete.c
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2021.03.04
 * @brief 
 * 
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#define SPI_MODE        5
#define ASAID           8
#define SPI_DUMMY       0x77

DiscStr_t test_disc;
FileStr_t test_file;
uint8_t CardID = 8;
uint8_t mem_addr[3] = {0, 0, 0};
uint8_t manu_cont[2] = {0, 0};
uint8_t fileID = 0;
char fileNAME[20];
uint8_t readdata[2] = {0};

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void) {
    // Setup
    C4M_DEVICE_set();
    DDRB |= 0x07;
    SPI_init();
    // FIXME: 待更新..
    Extern_CS_disable(ASAID);

    fileNAME[0] = 'T';
    fileNAME[1] = 'E';
    fileNAME[2] = 'S';
    fileNAME[3] = 'T';
    fileNAME[4] = '1';


    printf("Start test flash Manufacturer ID reading...\n");
    printf("=================\n");
    Flash_manufacture_code(manu_cont);
    for (uint8_t i = 0; i < 2; i++) {
        printf("manu_cont=>%x\n", manu_cont[i]);
    }
    int a=0xff;

    a = FlashFormat(&test_disc, CardID);
    printf("---Format complete!!!---, return = %d\n", a);

    a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);
    
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);
    
    //open File 1
    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    printf("---FileOpen---, return = %d\n", a);
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("FileName--0: %d\n", test_file.FileName[0]);
    printf("FileName--1: %d\n", test_file.FileName[1]);
    printf("FileName--2: %d\n", test_file.FileName[2]);
    printf("FileName--3: %d\n", test_file.FileName[3]);

    a = FileClose(&test_disc, &test_file);
    printf("---File Close--, return = %d\n", a);

    a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);

    //open File 2
    fileNAME[4] = '2';
    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    printf("---FileOpen---, return = %d\n", a);
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("FileName--0: %c\n", test_file.FileName[0]);
    printf("FileName--1: %c\n", test_file.FileName[1]);
    printf("FileName--2: %c\n", test_file.FileName[2]);
    printf("FileName--3: %c\n", test_file.FileName[3]);
    printf("FileName--4: %c\n", test_file.FileName[4]);

    a = FileClose(&test_disc, &test_file);
    printf("---File Close--, return = %d\n", a);

    a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);

    //open File 3 by name
    fileNAME[4] = '3';
    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    printf("---FileOpen---, return = %d\n", a);
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("FileName--0: %c\n", test_file.FileName[0]);
    printf("FileName--1: %c\n", test_file.FileName[1]);
    printf("FileName--2: %c\n", test_file.FileName[2]);
    printf("FileName--3: %c\n", test_file.FileName[3]);
    printf("FileName--4: %c\n", test_file.FileName[4]);

    a = FileClose(&test_disc, &test_file);
    printf("---File Close--, return = %d\n", a);

    a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);
    
    //Delte file 1 by name
    fileNAME[4] = '1';
    a = FileDelete(&test_disc, fileID, fileNAME);
    printf("---File Delete--, return = %d\n", a);
    
    a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);

    printf("-----After Delete-----\n");
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);

    //Delte file 2
    fileNAME[4] = '2';
    a = FileDelete(&test_disc, fileID, fileNAME);
    printf("---File Delete--, return = %d\n", a);
    
    a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);

    printf("-----After Delete-----\n");
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);

    printf("-----END-----\n");

    return 0;
}
