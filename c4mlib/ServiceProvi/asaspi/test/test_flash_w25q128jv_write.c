/**
 * @file test_flash_w25q128jv.c
 * @author Deng Xiang-Guan
 * @date 2019.10.02
 * @brief 使用SPI Master記憶傳輸和接收驅動函式測試w25q128jv flash IC。
 * 
 * 測試w25q128jv flash IC，傳送的封包為[command + memory address + data]，
 * 測試方式如下條列表示：
 *  1. 清除flash的資料。
 *  2. 寫入測試資料。
 *  3. 讀取測試資料是否正確。
 *  4. 讀取flash IC的ID是否和datasheet描述的一模一樣。
 *  5. 將測試資料更新。
 *  6. 如果測試都正確，成功計數增加並於終端機顯示成功訊息。
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#define SPI_MODE 5
#define ASAID 8

#define READ_MANU 0x90
#define read_command 0x03
#define read_id_command 0x90
#define dummy           0x00

#define SECTOR_ERASE_4K 0x20
#define WRITE_ENABLE 0x06
#define READ_DATA   0x03
#define PAGE_PROGRAM    0x02

uint32_t mem_addr = 0;
uint8_t manu_cont[3] = {0, 0, 0};
uint8_t test_rec[4] = {0, 0, 0 ,0};
uint8_t test_id[2] = {0, 0};
uint8_t test_write[3] = {0x11, 0x13, 0x11};
uint8_t addr[3] = {0, 0, 0};

int main() {
    // Setup
    // uint16_t success_cnt = 0;
    uint64_t ADDR = 0x777777;

    C4M_STDIO_init();
    SPIM_Inst.init();
    printf("Start test flash Read & Write...\n");
    ASABUS_ID_set(ASAID);
    while(true) {
        printf("=================\n");
        // SPIM_Inst.enable_cs(ASAID);
        // ASABUS_SPI_swap(0x06);  // ???
        // SPIM_Inst.disable_cs(ASAID);

        // SPIM_Mem_trm(SPI_MODE, ASAID, 0x20, 3, &mem_addr, 0, manu_cont);
        // _delay_ms(1);

        SPIM_Inst.enable_cs(ASAID);
        ASABUS_SPI_swap(dummy);  // ???
        SPIM_Inst.disable_cs(ASAID);

        SPIM_Mem_rec(SPI_MODE, ASAID, READ_MANU, 3, &mem_addr, 2,
                     manu_cont);

        // ASABUS_ID_set(ASAID);
        for (uint8_t i = 0; i < 2; i++) {
            printf("manu_cont=>%x\n", manu_cont[i]);
        }

        SPIM_Mem_trm(SPI_MODE, ASAID, WRITE_ENABLE, 0, &mem_addr, 0, manu_cont);
        printf("--WRITE_ENABLE--\n");

        addr[0] = (ADDR >> 16) & 0xFF;
        addr[1] = (ADDR >> 8) & 0xFF;
        addr[2] = ADDR & 0xFF;
        // SPIM_Mem_trm(SPI_MODE, ASAID, SECTOR_ERASE_4K, 3, &addr, 0, manu_cont);
        // printf("--SECTOR_ERASE_4K--\n");
        // // 50 ms
        // for (uint32_t i = 0; i < 80000; i++)
        //     __asm("nop");

        SPIM_Mem_trm(SPI_MODE, ASAID, PAGE_PROGRAM, 3, &addr, 3, test_write);
        printf("--PAGE_PROGRAM--\n");
        // 5 ms
        for (uint32_t i = 0; i < 8000; i++)
            __asm("nop");
        

        SPIM_Mem_rec(SPI_MODE, ASAID, READ_DATA, 3, &addr, 4, test_rec);
        printf("--READ_4BYTES_DATA--\n");
        // 5 ms
        for (uint32_t i = 0; i < 8000; i++)
            __asm("nop");

        // SPIM_Mem_rec(SPI_MODE, ASAID, read_command, 3, &mem_addr, sizeof(test_rec), test_rec);
        // // ASABUS_ID_set(ASAID);
        // for(uint8_t i=0;i<sizeof(test_rec);i++) {
        //     printf("->%d\n", test_rec[i]);
        // }

        // _delay_ms(2);

        // SPIM_Mem_rec(SPI_MODE, ASAID, read_id_command, 3, &mem_addr, sizeof(test_id), test_id);
        // ASABUS_ID_set(ASAID);
        // for(uint8_t i=0;i<sizeof(test_id);i++) {
        //     printf("_>%d\n", test_id[i]);
        // }
        // ASABUS_ID_set(ASAID);



        _delay_ms(2000);
    }

}
