/**
 * @file asaspi_remo_slave.c
 * @author Yi-Mou (sourslemon@g.ncu.edu.tw)
 * @brief
 * @date 2020.07.17
 *
 */
#include "c4mlib/ServiceProvi/remoptxrx/src/asaspi_remo_slave.h"

#include "c4mlib/C4MBios/debug/src/debug.h"

#define ASAUART_CMD_HEADER 0xAA  ///< @ingroup asauart_macro
#define ASAUART_RSP_HEADER 0xAB  ///< @ingroup asauart_macro

#define SPI_REMO_RWADDR    0
#define SPI_REMO_DATAWRITE 1
#define SPI_REMO_DATAREAD  2
#define SPI_REMO_WRITEOK   3
#define SPI_REMO_READOK    4

#define SPI_REMOP_HEADER   0
#define SPI_REMOP_BCDATA   1
#define SPI_REMOP_BCCHKSUM 2
#define SPI_REMOP_ULDATA   3
#define SPI_REMOP_ULOK     4
#define SPI_REMOP_DLDATA   5
#define SPI_REMOP_DLOK     6

#define SPI_REMO_OK 0
#define SPI_REMO_NG 1

#define SPI_REMOP_AllREGADD 255
#define SPI_REMOP_NONZERO   255

uint8_t SSpiRemoRX_step(void* void_p) {
    uint8_t InData       = 0;
    SSpiRemoStr_t* Str_p = (SSpiRemoStr_t*)void_p;

    switch (Str_p->State) {
        case SPI_REMO_RWADDR: {
            REGGET(Str_p->SpiReg_p, 1, &InData); /*Read Spi Port in Reg*/
            DEBUG_INFO("SSpiRemoRX_step() SPI_REMO_RWADDR \t read <%02X>\n",
                       InData);
            Str_p->RegAddr = InData & 0x7f;
            Str_p->RWMode  = (InData & 0x80) >> 7;

            if (Str_p->RWMode == 0) { /* Transmit Mode */
                Str_p->State = SPI_REMO_DATAWRITE;
            }
            else { /* Receive Mode */
                Str_p->OutData    = 0;
                Str_p->ErrorCount = 0;
                Str_p->Residual   = SPI_REMOP_NONZERO;
                Str_p->State      = SPI_REMO_DATAREAD;
            }
            break;
        }

        case SPI_REMO_DATAWRITE: {
            REGGET(Str_p->SpiReg_p, 1, &InData); /*Read Spi Port in Reg*/
            DEBUG_INFO("SSpiRemoRX_step() DATA \t read <%02X>\n", InData);
            if (RemoBF_temp(Str_p->RXRemoBF_p, Str_p->RegAddr, InData) == 0) {
                Str_p->State = SPI_REMO_WRITEOK;
            }
            break;
        }

        case SPI_REMO_DATAREAD: {
            REGGET(Str_p->SpiReg_p, 1, &InData);
            DEBUG_INFO(
                "SSpiRemoRX_step() Upline feedback DATA \t read <%02X>\n",
                InData);
            if (InData != Str_p->OutData) {
                Str_p->ErrorCount++;
            }
            if (Str_p->Residual > 0) {
                Str_p->Residual =
                    RemoBF_get(Str_p->TXRemoBF_p, Str_p->RegAddr,
                               &(Str_p->OutData)); /*Read Remobuffer */
                REGPUT(Str_p->SpiReg_p, 1, &(Str_p->OutData));
            }
            else { /* Remo buffer is empty*/
                REGPUT(Str_p->SpiReg_p, 1, &(Str_p->ErrorCount));
                Str_p->State = SPI_REMO_READOK;
            }
            break;
        }

        case SPI_REMO_WRITEOK: {
            REGGET(Str_p->SpiReg_p, 1, &InData);
            if (InData == SPI_REMO_OK) {
                RemoBF_put(Str_p->RXRemoBF_p, Str_p->RegAddr);
            }
            else {
                RemoBF_clr(Str_p->RXRemoBF_p);
            }
            Str_p->State = SPI_REMO_RWADDR;
            break;
        }
        case SPI_REMO_READOK: {
            Str_p->State = SPI_REMO_RWADDR;
            break;
        }
    }
    return 0;
}

uint8_t SSpiRemoPTXRX_step(void* void_p) {
    static uint8_t InData   = 0, OutData;
    uint8_t keeploop        = 0;
    static uint8_t rec_trig = 0;
    static uint8_t UL_last  = 0;
    static uint8_t old_temp = 0;
    SSpiRemoPStr_t* Str_p   = (SSpiRemoPStr_t*)void_p;
    do {
        keeploop = 0;
        switch (Str_p->State) {
            case SPI_REMOP_HEADER: {
                REGGET(Str_p->SpiReg_p, 1, &InData);
                if (InData == ASAUART_CMD_HEADER) {
                    Str_p->ChkSum = ASAUART_CMD_HEADER;
                    if (Str_p->BCRemoBF_p->DataTotal == 0) {
                        Str_p->State = SPI_REMOP_BCCHKSUM;
                    }
                    else {
                        Str_p->State = SPI_REMOP_BCDATA;
                    }
                }
                break;
            }
            case SPI_REMOP_BCDATA: {
                REGGET(Str_p->SpiReg_p, 1, &InData);
                if (RemoBF_temp(Str_p->BCRemoBF_p, SPI_REMOP_AllREGADD,
                                InData) == 0) {
                    Str_p->State = SPI_REMOP_BCCHKSUM;
                    rec_trig     = 1;
                }
                Str_p->ChkSum += InData;
                break;
            }
            case SPI_REMOP_BCCHKSUM: {
                REGGET(Str_p->SpiReg_p, 1, &InData);
                if (rec_trig == 1) {
                    REGFPT(Str_p->SpiReg_p, 0xff, 0, 0xff);
                    rec_trig = 0;
                }
                Str_p->OKNG = SPI_REMO_OK;
                if (Str_p->ULRemoBF_p->DataTotal == 0) {
                    Str_p->State = SPI_REMOP_ULOK;
                    OutData      = Str_p->OKNG;
                }
                else {
                    Str_p->State    = SPI_REMOP_ULDATA;
                    Str_p->Residual = RemoBF_get(Str_p->ULRemoBF_p,
                                                 SPI_REMOP_AllREGADD, &OutData);
                    Str_p->OldData  = 0;
                }

                if (InData == Str_p->ChkSum) {
                    RemoBF_put(Str_p->BCRemoBF_p, SPI_REMOP_AllREGADD);
                }
                else {
                    RemoBF_clr(Str_p->BCRemoBF_p);
                }
                break;
            }
            case SPI_REMOP_ULDATA: {
                REGGET(Str_p->SpiReg_p, 1, &(Str_p->FeedBack));
                REGPUT(Str_p->SpiReg_p, 1, &OutData);
                if (Str_p->OldData != Str_p->FeedBack) {
                    Str_p->OKNG += 1;
                }
                if (UL_last == 1) {
                    Str_p->State = SPI_REMOP_ULOK;
                    OutData      = Str_p->OKNG;
                    UL_last      = 0;
                    break;
                }
                Str_p->OldData = old_temp;
                old_temp       = OutData;
                if (Str_p->Residual == 0) {
                    UL_last = 1;
                    OutData = 0;
                }
                else {
                    Str_p->Residual = RemoBF_get(Str_p->ULRemoBF_p,
                                                 SPI_REMOP_AllREGADD, &OutData);
                }
                break;
            }
            case SPI_REMOP_ULOK: {
                REGGET(Str_p->SpiReg_p, 1, &(Str_p->FeedBack));
                if (rec_trig == 0) {
                    REGPUT(Str_p->SpiReg_p, 1, &OutData);
                    rec_trig = 1;
                }
                else {
                    if (Str_p->DLRemoBF_p->DataTotal == 0) {
                        Str_p->State = SPI_REMOP_DLOK;
                    }
                    else {
                        Str_p->State    = SPI_REMOP_DLDATA;
                        Str_p->Residual = SPI_REMOP_NONZERO;
                    }
                    rec_trig = 0;
                }
                break;
            }
            case SPI_REMOP_DLDATA: {
                REGGET(Str_p->SpiReg_p, 1, &InData);
                Str_p->Residual =
                    RemoBF_temp(Str_p->DLRemoBF_p, SPI_REMOP_AllREGADD, InData);
                if (Str_p->Residual == 0) {
                    Str_p->State = SPI_REMOP_DLOK;
                }
                break;
            }
            case SPI_REMOP_DLOK: {
                REGGET(Str_p->SpiReg_p, 1, &InData);
                if (InData == SPI_REMO_OK) {
                    RemoBF_put(Str_p->DLRemoBF_p, SPI_REMOP_AllREGADD);
                }
                else {
                    RemoBF_clr(Str_p->DLRemoBF_p);
                }
                DEBUG_INFO("dlok %x\n", InData);
                Str_p->State    = SPI_REMOP_HEADER;
                Str_p->FeedBack = 0;
                Str_p->OKNG     = 0;
                Str_p->Residual = 0;
                Str_p->OldData  = 0;
                break;
            }
        }
    } while (keeploop);
    return 0;
}
