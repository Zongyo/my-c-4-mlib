/**
 * @file test_mspi_remop.c
 * @author Yi-Mou (sourslemon@g.ncu.edu.tw)
 * @brief 
 * @date 2020.09.14
 * 
 * 測試mspi_remo功能 需與test_sspi_remop共同測試
 * 兩邊將交換一次資料
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/hwimp/src/layout_macro.h"
#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/config/spi.cfg"
#include "c4mlib/ServiceProvi/remoptxrx/src/asaspi_remo_master.h"
#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/C4MBios/hardwareset/src/spi_set.h"

#define BCDDEPTH  2
#define BCVSDEPTH 1
#define MAXCARD   1
#define DLDDEPTH  2
#define DLVSDEPTH 1
#define ULDDEPTH  2
#define ULVSDEPTH 1
#define PERIPHERAL_INTERRUPT_ENABLE 255

uint8_t spi_swap(uint8_t data);
uint8_t test_int(void *p);

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPISETDATALISTINI;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void) {
    C4M_DEVICE_set();
    printf("start!\n");
    SPI_init();
    
    MSPIREMOP_LAY(remotest, &SPDR, BCBFSTR, BCDDEPTH, BCVSDEPTH, MAXCARD);
    MSPIREMOP_REG(remotest, dlbtest, DLDDEPTH, DLVSDEPTH, ulbtest, ULDDEPTH, ULVSDEPTH);

    uint16_t bcdata = 0x5400;
    uint16_t dldata = 0x4747;
    uint16_t updata = 0;

    RemoBF_reg(&BCBFSTR, &bcdata, 2);
    RemoBF_reg(&dlbtest, &dldata, 2);
    RemoBF_reg(&ulbtest, &updata, 2);
     
    SPIOpStr_t HWOPSTR = SPIOPSTRINI;
    SPIHWINT_LAY(SpiInt_Str, 0, 2, HWOPSTR);
    remotest.SpiTaskId = HWInt_reg(&SpiInt_Str,&MSpiRemoPTXRX_step, &remotest);
    remotest.SpiHWIntStr_ssp = &SpiInt_Str;

    char test_ID = HWInt_reg(&SpiInt_Str,&test_int,0);
    HWInt_en(&SpiInt_Str,PERIPHERAL_INTERRUPT_ENABLE,1);
    HWInt_en(&SpiInt_Str,test_ID,1);

    sei();
    MSpiRemoPTXRX_step(&remotest);
    while(1){
        printf("uldata = %X\n",updata);
        _delay_ms(200);
	}
    return 0;
}

uint8_t test_int(void *p){
    return 0;
}
