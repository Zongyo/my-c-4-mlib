/**
 * @file pwm_imp.c
 * @author maxwu84
 * @brief
 * @date 2019.09.04
 *
 */

#include "c4mlib/ServiceProvi/hwimp/src/m128/pwm_imp.h"

// PwmIntStrList_p is used in std_isr
HWIntStr_t *PwmIntStrList_p[PWM_HW_NUM];
