/**
 * @file adc_imp.c
 * @author LiYu87
 * @date 2019.09.04
 * @brief m128 adc 實現
 *
 */

#include "c4mlib/ServiceProvi/hwimp/src/m128/adc_imp.h"

// AdcIntStrList_p is used in std_isr
HWIntStr_t *AdcIntStrList_p[ADC_HW_NUM];
