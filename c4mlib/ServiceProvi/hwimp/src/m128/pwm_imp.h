/**
 * @file pwm_imp.h
 * @author maxwu84
 * @brief
 * @date 2019.09.04
 *
 */

#ifndef C4MLIB_HARDWARE2_M128_PWM_IMP_H
#define C4MLIB_HARDWARE2_M128_PWM_IMP_H

#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"

/* Public Section Start */
#define PWM_HW_NUM 4
extern HWIntStr_t *PwmIntStrList_p[PWM_HW_NUM];
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_M128_PWM_IMP_H
