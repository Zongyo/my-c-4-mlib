/**
 * @file layout_macro.h
 * @author ya058764
 * @brief 
 * @date 2020.05.06
 * 
 */

#ifndef C4MLIB_HWIMP_LAYOUT_H
#define C4MLIB_HWIMP_LAYOUT_H

#if defined(__AVR_ATmega128__)
#    include "m128/layout_macro.h"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/layout_macro.h"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/layout_macro.h"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif

#endif  // C4MLIB_HWIMP_LAYOUT_H
