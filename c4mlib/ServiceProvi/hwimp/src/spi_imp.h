/**
 * @file spi_imp.h
 * @author maxwu84
 * @date 2019.09.04
 * @brief
 */

#ifndef C4MLIB_HARDWARE2_SPI_INT_SET_H
#define C4MLIB_HARDWARE2_SPI_INT_SET_H

#if defined(__AVR_ATmega128__)
#    include "m128/spi_imp.h"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/spi_imp.h"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/spi_imp.h"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif

#endif
