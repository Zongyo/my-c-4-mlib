/**
 * @file test_uart_master_card.c
 * @author s915888
 * @date 2021.05.25
 * @brief 測試 ASA單板電腦 作為Master，透過Uart通訊，傳送接收封包。
 *
 * 需與 test_uart_slave_card.c 一起做測試，
 * 硬體配置:
 * >>   將Master與Slave的TXRX對接，並且雙方共地
 * 程式執行步驟:
 * >>   宣告UARTMStr_t及UARTOpStr_t結構體
 * >>   設定UART相關硬體暫存器
 * >>   使用UARTM0STR_LAY將UARTOpStr_t結構體登錄進UARTMStr_t
 * 執行成功結果:
 * >>   Master transmit 3 bytes data to Slave
 * >>  【Test 1】 UARTM_trm : data[0]=5
 * >>  【Test 1】 UARTM_trm : data[1]=6
 * >>  【Test 1】 UARTM_trm : data[2]=7
 * >>  【Test 1】 UARTM_trm : data[3]=0
 * >>  【Test 1】 UARTM_trm : data[4]=0
 * >>   Master transmit 3 bytes data to Slave
 * >>  【Test 2】 UARTM_trm : data[0]=5
 * >>  【Test 2】 UARTM_trm : data[1]=6
 * >>  【Test 2】 UARTM_trm : data[2]=7
 * >>  【Test 2】 UARTM_trm : data[3]=0
 * >>  【Test 2】 UARTM_trm : data[4]=0
 * 執行失敗結果:
 * >>   [Test 1]UARTM_trm Fail [錯誤碼]
 * >>  【Test 1】 UARTM_trm : data[0]=5
 * >>  【Test 1】 UARTM_trm : data[1]=6
 * >>  【Test 1】 UARTM_trm : data[2]=7
 * >>  【Test 1】 UARTM_trm : data[3]=0
 * >>  【Test 1】 UARTM_trm : data[4]=0
 * >>   [Test 1]UARTM_trm Fail [錯誤碼]
 * >>  【Test 2】 UARTM_trm : data[0]=5
 * >>  【Test 2】 UARTM_trm : data[1]=6
 * >>  【Test 2】 UARTM_trm : data[2]=7
 * >>  【Test 2】 UARTM_trm : data[3]=0
 * >>  【Test 2】 UARTM_trm : data[4]=0
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardware/src/isr.h"
#include "c4mlib/ServiceProvi/asauart/src/asauart_master_card.h"
#include "c4mlib/ServiceProvi/time/src/hal_time.h"

#include <util/delay.h>

int main() {
    C4M_DEVICE_set();
    
    UARTMStr_t uart_str_p = UART1REGPARA;
    UARTM_Inst.init();
    sei();

    uint8_t data_buffer[5] = {5, 6, 7};
    uint8_t result         = 0;

    /***** UARTM Mode0 Transmit test *****/

    printf(" Master transmit 1 bytes data to Slave \n");

    result = UARTM0_trm(&uart_str_p, 0, 128, 1, 1, data_buffer);

    if (result) {
        printf("[Test 1]UARTM_trm Fail [%d]\n", result);
    }
    for (int i = 0; i < 5; i++) {
        printf("[Test 1] UARTM_trm : data[%u]=%u\n", i, data_buffer[i]);
    }

    printf(" Master transmit 3 bytes data to Slave \n");
    result = UARTM0_trm(&uart_str_p, 0, 128, 3, 3, data_buffer);

    if (result) {
        printf("[Test 2]UARTM_trm Fail [%d]\n", result);
    }
    for (int i = 0; i < 5; i++) {
        printf("[Test 2] UARTM_trm : data[%u]=%u\n", i, data_buffer[i]);
    }

    while (1) {
    }
}
