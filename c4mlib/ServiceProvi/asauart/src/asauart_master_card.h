/**
 * @file asauart_master_card.h
 * @author s915888
 * @date 2021.05.25
 * @brief 放置 ASA UART Master 0 通訊函式及Marco
 */

#ifndef C4MLIB_ASAUART_MASTER_CARD_H
#define C4MLIB_ASAUART_MASTER_CARD_H

#include "c4mlib/C4MBios/macro/src/std_res.h"

#include <stdint.h>

/**
 * @defgroup asauartS0_func
 * @defgroup asauartS0_struct
 * @defgroup asauartS0_macro
 */

#define ASAUART_CMD_HEADER 0xAA  ///< @ingroup asauart_macro
#define ASAUART_RSP_HEADER 0xAB  ///< @ingroup asauart_macro
/* Public Section Start */
#define UART1REGPARA                                                           \
    {                                                                          \
        .DataReg_p = &UDR1, .UARTDataBytes = 1, .TXRXSwiReg_p = &PORTF,        \
        .TXRXSwiMask = 0x01, .TXRXSwiShift = 4, .TXEnReg_p = &UCSR1B,          \
        .TXEnMask = 0x10, .TXEnShift = 3, .RXEnReg_p = &UCSR1B,                \
        .RXEnMask = 0x40, .RXEnShift = 4, .TXIntEnReg_p = &UCSR1B,             \
        .TXIntEnMask = 0x40, .TXIntEnShift = 6, .RXIntEnReg_p = &UCSR1B,       \
        .RXIntEnMask = 0x80, .RXIntEnShift = 7, .TXIntClrReg_p = &UCSR1A,      \
        .TXIntClrMask = 0x40, .TXIntClrShift = 6, .RXIntClrReg_p = &UCSR1A,    \
        .RXIntClrMask = 0x80, .RXIntClrShift = 7, .TXIntSetReg_p = &UCSR1A,    \
        .TXIntSetMask = 0x40, .TXIntSetShift = 6, .RXIntSetReg_p = &UCSR1A,    \
        .RXIntSetMask = 0x80, .RXIntSetShift = 7, .TXIntFReg_p = &UCSR1A,      \
        .TXIntFMask = 0X20, .TXIntFShift = 5, .RXIntFReg_p = &UCSR1A,          \
        .RXIntFMask = 0x80, .RXIntFShift = 7, .TimMax = 500000,                \
        .Header = ASAUART_CMD_HEADER                                           \
    }
/**
 * @brief ASA UART Master0 結構初始化巨集
 * @ingroup asauartS0_macro
 */
typedef struct {
    volatile uint8_t* DataReg_p;
    uint8_t UARTDataBytes;
    volatile uint8_t* TXEnReg_p;
    uint8_t TXEnMask, TXEnShift;
    volatile uint8_t* RXEnReg_p;
    uint8_t RXEnMask, RXEnShift;
    volatile uint8_t* TXRXSwiReg_p;
    uint8_t TXRXSwiMask, TXRXSwiShift;
    volatile uint8_t* TXIntEnReg_p;
    uint8_t TXIntEnMask, TXIntEnShift;
    volatile uint8_t* RXIntEnReg_p;
    uint8_t RXIntEnMask, RXIntEnShift;
    volatile uint8_t* TXIntClrReg_p;
    uint8_t TXIntClrMask, TXIntClrShift;
    volatile uint8_t* RXIntClrReg_p;
    uint8_t RXIntClrMask, RXIntClrShift;
    volatile uint8_t* TXIntSetReg_p;
    uint8_t TXIntSetMask, TXIntSetShift;
    volatile uint8_t* RXIntSetReg_p;
    uint8_t RXIntSetMask, RXIntSetShift;
    volatile uint8_t* TXIntFReg_p;
    uint8_t TXIntFMask, TXIntFShift;
    volatile uint8_t* RXIntFReg_p;
    uint8_t RXIntFMask, RXIntFShift;
    uint8_t Header;
    uint32_t TimMax;
} UARTMStr_t;
/**
 * @brief ASA UART Master0 硬體結構初始化巨集
 * @ingroup asauartS0_macro
 */
#define UARTM0STR_LAY(UARTREMOSTR, UARTOPSTR)                                  \
    {                                                                          \
        UARTREMOSTR.DataReg_p   = UARTOPSTR.UARTDataReg_p;                     \
        UARTREMOSTR.TXIntFReg_p = UARTOPSTR.TXIntFReg_p;                       \
        UARTREMOSTR.TXIntFMask  = UARTOPSTR.TXIntFMask;                        \
        UARTREMOSTR.TXIntFShift = UARTOPSTR.TXIntFShift;                       \
        UARTREMOSTR.RXIntFReg_p = UARTOPSTR.RXIntFReg_p;                       \
        UARTREMOSTR.RXIntFMask  = UARTOPSTR.RXIntFMask;                        \
        UARTREMOSTR.RXIntFShift = UARTOPSTR.RXIntFShift;                       \
    }
/**
 * @brief ASA UART Master0 多位元組傳送函式。
 * @ingroup asauartS_func
 *
 * @param Mode      UART 通訊模式，目前支援0, 3
 * @param CardId    UART僕ID：      目標的裝置UART ID
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Data_p    待收資料指標。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 255： Timeout。
 *                   - 1~8： 對應State錯誤。
 *
 * ASA 作為 Master端 透過UART通訊傳送資料。
 * Uart Master端 傳送給 Slave端，依照封包不同，分作2種 Mode ，如以下所示：
 *  - Mode 0：
 *      封包組成 為
 *      [Header]、[CardId]、[RegAdd]、[Bytes]、[Data]、[ChkSum]、[Rec(Header)]、[Rec(Resp)]、[Rec(ChkSum)]。
 *      先後傳送資料 [HEADER
 *      (0xAA)]、[CardId]、[RegAdd]、[Bytes]，隨後再根據
 *      資料筆數[Bytes]由低到高丟出資料，傳完資料後會傳送所有已傳送資料加總
 *      [checksum] 給Slave端驗證，Slave會回傳當作 [HEADER(0xAB)]
 *      成功資訊或錯誤資訊。
 *  - Mode 3：
 *      封包組成 為 [Data]，單純送收資料。
 *      若Bytes=0則為ATcommand模式，並且先送資料再從Slave接收資料
 *
 */
uint8_t UARTM0_trm(void* void_p, uint8_t Mode, uint8_t CardId, uint8_t RegAdd,
                   uint8_t Bytes, void* Data_p);
uint8_t UARTM0_rec(void* void_p, uint8_t Mode, uint8_t CardId, uint8_t RegAdd,
                   uint8_t Bytes, void* Data_p);
/**
 * @brief ASA UART Master0 旗標式傳送函式。
 * @ingroup asauartS_func
 * @param Mode      UART 通訊模式，目前支援0, 3
 * @param CardId    UART僕ID：      目標的裝置UART ID
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Mask      位元組遮罩。
 * @param shift     接收旗標向右位移。
 * @param Data_p    待收資料指標。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 255： Timeout。
 *                   - 1~8： 對應State錯誤。
 *
 * Uart Master0 旗標式資料傳送是由UARTM0_rec()以及UARTM0_trm()實現。
 * 依照功能將只支援指定Mode，通訊方式如下：
 *  - Mode 0：
 *      使用UARTM0_rec() Mode 0 讀取指定UID、RegAdd中的資料，
 *      將資料左移後，用遮罩取資料，最後使用UARTM0_trm() Mode 0
 *      傳輸資料到指定RegAdd。
 *  - Mode 3：
 *      使用UARTM_rec() Mode 3 讀取資料，將資料左移後，
 *      用遮罩取資料，最後使用UARTM_trm() Mode 3 傳輸資料。
 *
 */
uint8_t UARTM0_ftm(void* void_p, uint8_t Mode, uint8_t CardId, uint8_t RegAdd,
                   uint8_t Mask, uint8_t shift, uint8_t Data);
uint8_t UARTM0_frc(void* void_p, uint8_t Mode, uint8_t CardId, uint8_t RegAdd,
                   uint8_t Mask, uint8_t shift, uint8_t* Data_p);
/* Public Section End */
#endif  // C4MLIB_ASAUART_MASTER_UART0_H
