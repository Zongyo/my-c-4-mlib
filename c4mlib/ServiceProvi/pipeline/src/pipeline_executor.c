/**
 * @file pipeline_executor.c
 * @author cy023, ya058764
 * @date 2021.5.30
 * @brief
 */

#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"
#include "c4mlib/C4MBios/debug/src/debug.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>

#define ERRORCODE               255
#define PIPELINE_TASK_RUN_OK    0

uint8_t Pipeline_reg(PipelineStr_t* PLStr_ssp, TaskFunc_t FbFunc_p, void* FbPara_p,
                     uint8_t* TaskName_p) {
    uint8_t TaskId                     = PLStr_ssp->Total + 1; //avoid number 0
    PLStr_ssp->Task_p[TaskId].state    = 0;
    PLStr_ssp->Task_p[TaskId].FBFunc_p = FbFunc_p;
    PLStr_ssp->Task_p[TaskId].FBPara_p = FbPara_p;

    // TODO: TickStart(), TickStop()
    DEBUG_INFO("TaskId:%d, Name:%s", TaskId, TaskName_p);

    if (PLStr_ssp->Total == PLStr_ssp->MaxTask) {
        DEBUG_INFO("Pipeline_reg Over Booking Error\n");
        return ERRORCODE;
    }
    else {
        DEBUG_INFO("TaskId:%d, Name:%s\n", TaskId, TaskName_p);
        PLStr_ssp->Total++;
        return TaskId;
    }
}

uint8_t Pipeline_step(void* void_p) {
    volatile PipelineStr_t* PLStr_ssp = (PipelineStr_t*)void_p;
    uint8_t ErrorCode = 0;
    uint8_t TaskId;
    if (FifoBF_get((FifoBFStr_t*)(PLStr_ssp->FifoBF_p), (void*)&TaskId, 1) >=
        0) {
        switch (PLStr_ssp->Task_p[TaskId].state) {
            case 0: {
                DEBUG_INFO("Pipeline_step() Error Code : NonTrig\n");
                break;
            }
            case 1: {
                if (PLStr_ssp->TaskId){
                    ScopeBF_put((ScopeBFStr_t*)(PLStr_ssp->ScopeBF_p),
                                (void*)&TaskId, 1);
                    DEBUG_INFO("Pipeline_step() Error Code : TriggerOneTime\n");
                    PLStr_ssp->Task_p[TaskId].state = 0;
                    ErrorCode = PLStr_ssp->Task_p[TaskId].FBFunc_p(
                        PLStr_ssp->Task_p[TaskId].FBPara_p);
                }
                break;
            }
            default: {
                DEBUG_INFO("Pipeline_step() Error Code : OverTrig\n");
                break;
            }
        }
        if (ErrorCode != PIPELINE_TASK_RUN_OK) {
            ScopeBF_tri(PLStr_ssp->ScopeBF_p);
            ScopeBF_full(PLStr_ssp->ScopeBF_p);
            HWInt_en(PLStr_ssp->HWIStr_ssp, 255, 0);
        }
    }
    return ErrorCode;
}
