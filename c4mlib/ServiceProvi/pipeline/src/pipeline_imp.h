/**
 * @file pipeline_imp.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2020.10.06
 * @brief 
 * 
 */

#ifndef PIPELINE_IMP_H
#define PIPELINE_IMP_H
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_imp.h"

/* Public Section Start */
extern PipelineStr_t SysPipeline_str;
/* Public Section End */
#endif//PIPELINE_IMP_H
