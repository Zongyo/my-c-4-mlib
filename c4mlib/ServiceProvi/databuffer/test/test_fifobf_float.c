/**
 * @file test_fifobf_float.c
 * @author YP-Shen
 * @date 2020-03-17
 * @brief  測試FifoBF_put及FifoBF_get緩衝float型態的資料
 */

#include "c4mlib/ServiceProvi/databuffer/src/fifobf.h"
#include "c4mlib/C4MBios/device/src/device.h"

#define FIFO_BUF_DATA_DEPTH_FTASK 28

int main(void) {
    C4M_STDIO_init();
    FIFOBUFF_LAY(FBF_p, FIFO_BUF_DATA_DEPTH_FTASK);  // sizeof(pdata)=28

    float pdata[7] = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.998};
    float gdata[3] = {0};

    printf("Return of FifoBF_put : %d\n",
           FifoBF_put(&FBF_p, pdata, sizeof(pdata)));
    printf("put_data = [");
    for (int i = 0; i < FBF_p.Depth / sizeof(float); i++) {
        printf("%g, ", *((float*)(FBF_p.List_p) + i));
    }
    printf("]\n");
    printf("Return of FifoBF_get : %d\n",
           FifoBF_get(&FBF_p, gdata, sizeof(gdata)));
    printf("get_data = [");
    for (int i = 0; i < sizeof(gdata) / sizeof(float); i++) {
        printf("%g, ", gdata[i]);
    }
    printf("]\n");

    printf("Get again:\n");
    printf("Return of FifoBF_get : %d\n",
           FifoBF_get(&FBF_p, gdata, sizeof(gdata)));
    printf("get_data = [");
    for (int i = 0; i < sizeof(gdata) / sizeof(float); i++) {
        printf("%g, ", gdata[i]);
    }
    printf("]\n");
    printf("Return of FifoBF_clr : %d\n", FifoBF_clr(&FBF_p));
    printf("END\n");
}
