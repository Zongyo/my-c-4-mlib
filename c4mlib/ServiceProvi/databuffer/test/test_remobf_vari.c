/**
 * @file test_remobf_vari.c
 * @author Yi-Mou
 * @brief 提供操作remobf個別編號變數的測試
 * @date 2020.03.24
 *
 */

#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/C4MBios/device/src/device.h"

int main() {
    C4M_DEVICE_set();
    REMOBUFF_LAY(RemoBF_Str, 10, 3); //建置remo結構並宣告buffer空間大小

    uint8_t PID_local[3] = {0};
    uint32_t speed_local = 0;
    /*將local變數登錄至remoBF結構體*/
    RemoBF_reg(&RemoBF_Str, PID_local, 3);
    RemoBF_reg(&RemoBF_Str, &speed_local, 4);

    uint8_t PID_remo[3] = {3, 18, 8};
    uint32_t speed_remo = 23156;

    /* 將模擬的PID遠端數值寫進remobf結構體中對應編號的緩衝區 */
    RemoBF_temp(&RemoBF_Str, 0, PID_remo[0]);
    RemoBF_temp(&RemoBF_Str, 0, PID_remo[1]);
    RemoBF_temp(&RemoBF_Str, 0, PID_remo[2]);

    /* 將對應編號緩衝區的內容寫至登錄的PID位址之中 */
    RemoBF_put(&RemoBF_Str, 0);
    printf("%d , %d ,%d\n", PID_local[0], PID_local[1], PID_local[2]);

    /* 透過編號讀取本地端的變數內容 */
    RemoBF_get(&RemoBF_Str, 1, (uint8_t*)(&speed_remo));
    RemoBF_get(&RemoBF_Str, 1, (uint8_t*)(&speed_remo) + 1);
    RemoBF_get(&RemoBF_Str, 1, (uint8_t*)(&speed_remo) + 2);
    RemoBF_get(&RemoBF_Str, 1, (uint8_t*)(&speed_remo) + 3);

    printf("%ld\n", speed_remo);
}
