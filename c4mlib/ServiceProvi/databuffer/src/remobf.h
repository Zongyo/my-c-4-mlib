/**
 * @file remobf.c
 * @author Yi-Mou , jerryhappyball
 * @date 2020.04.22
 * @brief 定義RemoBuffer結構與函式型態
 */

#ifndef C4M_DATABF_REMOBF_H
#define C4M_DATABF_REMOBF_H

#include <stddef.h>
#include <stdint.h>

/**
 * @defgroup remobf_macro
 * @defgroup remobf_func
 * @defgroup remobf_struct
 */
/* Public Section Start */
#define NETRBF(task, num, buffer_p)                                            \
    task.RBF[num] = buffer_p  ///< 供管理結構體鏈結到串列通訊工作的結構體

/**
 * @brief 供使用者創建並連結實體空間至緩衝器結構體中
 * @ingroup rtpio_macro
 */
#define REMOBUFF_LAY(RBSTR, DDEPTH, VDEPTH)                                    \
    static RemoBFStr_t RBSTR = {0};                                            \
    {                                                                          \
        static uint8_t RBSTR##_DLIST[DDEPTH]    = {0};                         \
        static RemoVari_t RBSTR##_VLIST[VDEPTH] = {0};                         \
        RBSTR.DList_p                           = RBSTR##_DLIST;               \
        RBSTR.VList_p                           = RBSTR##_VLIST;               \
    }                                                                          \
    RBSTR.DataDepth = DDEPTH;                                                  \
    RBSTR.VariDepth = VDEPTH;

/**
 * @brief 提供remo變數位址連結及資訊儲存
 * @ingroup remobf_struct
 */
typedef struct {
    uint8_t Bytes;  ///< 變數位元數
    void* Vari_p;   ///< 變數位址
} RemoVari_t;

/**
 * @brief 提供remobf結構原形
 * @ingroup remobf_struct
 */
typedef struct {
    uint8_t State;        ///< Buffer狀態
    uint8_t DataIndex;    ///< 資料讀取索引
    uint8_t VariIndex;    ///< 變數讀取索引
    uint8_t ByteIndex;    ///< 位元讀取索引
    uint8_t DataTotal;    ///< 資料總位元數
    uint8_t VariTotal;    ///< 變數總數
    uint8_t DataDepth;    ///< Buffer最大總位元數,
    uint8_t VariDepth;    ///< Remo變數最大總數量,
    RemoVari_t* VList_p;  ///< 變數儲存空間指標
    uint8_t* DList_p;     ///< Buffer空間指標
} RemoBFStr_t;

/**
 * @brief 將要提供遠端讀寫的變數大小及位址寫入結構體中並回傳遠端讀寫編號
 *
 * @ingroup remobf_func
 * @param Str_p remobf結構指標
 * @param Data_p 變數位址
 * @param Bytes 變數位元數
 * @return uint8_t 遠端讀寫編號(0~254)
 */
uint8_t RemoBF_reg(RemoBFStr_t* Str_p, void* Data_p, uint8_t Bytes);

/**
 * @brief 將buffer內的資料寫入至對應的讀寫變數位址中
 *
 * @ingroup remobf_func
 * @param Str_p remobf結構指標
 * @param RegId 對應變數的編號 0~254:對應編號 255:所有已註冊變數
 * @return uint8_t 錯誤代碼
 * - 0: 正常
 * - 1: State錯誤Buffer尚未填滿
 */
uint8_t RemoBF_put(RemoBFStr_t* Str_p, uint8_t RegId);

/**
 * @brief 將1byte的資料填入至buffer中
 *
 * @ingroup remobf_func
 * @param Str_p remobf結構指標
 * @param RegId 對應變數的編號 0~254:對應編號 255:所有已註冊變數
 * @param Data 欲寫入之資料值
 * @return int8_t 回傳剩餘Bytes數
 */
int8_t RemoBF_temp(RemoBFStr_t* Str_p, uint8_t RegId, uint8_t Data);

/**
 * @brief 取得1byte對應編碼變數中的值
 *
 * @ingroup remobf_func
 * @param Str_p remobf結構指標
 * @param RegId 對應變數的編號 0~254:對應編號 255:所有已註冊變數
 * @param Data_p 放入取得值的位址
 * @return int8_t 回傳剩餘Bytes數
 */
int8_t RemoBF_get(RemoBFStr_t* Str_p, uint8_t RegId, void* Data_p);

/**
 * @brief 歸零remobf結構體中的資料索引
 *
 * @ingroup remobf_func
 * @param Str_p Str_p remobf結構指標
 * @return uint8_t 錯誤代碼 0: 正常
 */
uint8_t RemoBF_clr(RemoBFStr_t* Str_p);
/* Public Section End */
#endif /* C4M_DATABF_REMOBF_H */
