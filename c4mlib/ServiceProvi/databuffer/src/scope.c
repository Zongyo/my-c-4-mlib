/**
 * @file scope.c
 * @author LCY , jerryhappyball
 * @date 2020.04.22
 * @brief scope buffer 實作
 */

#include "c4mlib/ServiceProvi/databuffer/src/scope.h"

#include "c4mlib/C4MBios/device/src/device.h"

int8_t ScopeBF_put(ScopeBFStr_t* ScopeBF_p, void* Data_p, uint8_t Bytes) {
    if (ScopeBF_p->State != AftTrig)
        return (StateErrorCode);  // Return error code
    if (Bytes > ScopeBF_p->Depth - ScopeBF_p->Total) {
        ScopeBF_p->State = Wait4Read;
        return (ScopeBF_p->Depth - (ScopeBF_p->Total + Bytes));
    }
    uint8_t sreg_temp = SREG;
    cli();
    for (uint8_t i = 0; i < Bytes; i++) {
        ((uint8_t*)ScopeBF_p->List_p)[ScopeBF_p->Pindex] =
            ((uint8_t*)Data_p)[i];
        ScopeBF_p->Pindex++;
        if (ScopeBF_p->Pindex == ScopeBF_p->Depth) {
            ScopeBF_p->Pindex = 0;
        }
        ScopeBF_p->Total++;
    }
    if (ScopeBF_p->Total == ScopeBF_p->Depth) {
        ScopeBF_p->State = Wait4Read;
        SREG             = sreg_temp;
        return (ScopeBF_p->Total);
    }
    else {
        SREG = sreg_temp;
        return (ScopeBF_p->Depth - ScopeBF_p->Total);
    }
}

int8_t ScopeBF_tri(ScopeBFStr_t* ScopeBF_p) {
    if (ScopeBF_p->State == AftTrig)
        return (StateErrorCode);
    ScopeBF_p->State  = AftTrig;
    ScopeBF_p->Total  = ScopeBF_p->PreCount;
    ScopeBF_p->Gindex = ScopeBF_p->Pindex - ScopeBF_p->PreCount;
    if (ScopeBF_p->Pindex < ScopeBF_p->PreCount) {
        ScopeBF_p->Gindex   = ScopeBF_p->Pindex - ScopeBF_p->PreCount;
        ScopeBF_p->PreCount = ScopeBF_p->Gindex;
    }
    else if (ScopeBF_p->Gindex < 0) {
        ScopeBF_p->Gindex =
            ScopeBF_p->Pindex - ScopeBF_p->PreCount + ScopeBF_p->Depth;
    }
    return (ScopeBF_p->Total);
}

int8_t ScopeBF_get(ScopeBFStr_t* ScopeBF_p, void* Data_p, uint8_t Bytes) {
    if (ScopeBF_p->State != Wait4Read)
        return (StateErrorCode);
    if (ScopeBF_p->Total < Bytes) {
        ScopeBF_p->State = BefTrig;
        return (ScopeBF_p->Total - Bytes);
    }
    uint8_t sreg_temp = SREG;
    cli();
    for (uint8_t i = 0; i < Bytes; i++) {
        ((uint8_t*)Data_p)[i] =
            ((uint8_t*)ScopeBF_p->List_p)[ScopeBF_p->Gindex];
        ScopeBF_p->Gindex++;
        if (ScopeBF_p->Gindex == ScopeBF_p->Depth) {
            ScopeBF_p->Gindex = 0;
        }
        ScopeBF_p->Total--;
    }
    if (ScopeBF_p->Total == 0) {
        ScopeBF_p->State = BefTrig;
    }
    SREG = sreg_temp;
    return (ScopeBF_p->Total);
}

uint8_t ScopeBF_full(ScopeBFStr_t* ScopeBF_p) {
    ScopeBF_p->State = Wait4Read;
    return (ScopeBF_p->Total);
}

uint8_t ScopeBF_clr(ScopeBFStr_t* ScopeBF_p) {
    ScopeBF_p->Total  = 0;
    ScopeBF_p->Pindex = 0;
    ScopeBF_p->Gindex = 0;
    ScopeBF_p->State  = BefTrig;
    return (ScopeBF_p->Total);
}
