/**
 * @file scope.h
 * @author LCY , jerryhappyball
 * @date 2020.04.22
 * @brief scope buffer 定義
 */

#ifndef C4MLIB_DATABUFFER_SCOPEBUFFER_H
#define C4MLIB_DATABUFFER_SCOPEBUFFER_H

#include "c4mlib/C4MBios/device/src/device.h"

#include <stdint.h>
#include <string.h>
/* Public Section Start */
// FIXME: 註解依照coding sytle更正
#define BefTrig        0  //觸發前
#define AftTrig        1  //己觸發
#define Wait4Read      2  //緩衝區己滿可讀
#define StateErrorCode -127

#define NETCBF(task_p, num, buffer_p) task_p→CBF[num] = buffer_p
#define SCOPEBUFF_LAY(SBSTR, MAXNUM, PreC)                                     \
    static ScopeBFStr_t SBSTR = {                                                     \
        .State    = BefTrig,                                                   \
        .Pindex   = 0,                                                         \
        .Gindex   = 0,                                                         \
        .Total    = 0,                                                         \
        .Depth    = MAXNUM,                                                    \
        .PreCount = PreC,                                                      \
    };                                                                         \
    {                                                                          \
        static uint8_t List[MAXNUM] = {0};                                     \
        SBSTR.List_p                = List;                                    \
    }

/**
 * @brief 定義buffer結構成員
 *
 */
typedef struct {
    uint8_t State;     // state of Buffer
    int16_t Pindex;    // Matrix Put Index of Buffer
    int16_t Gindex;    // Matrix Get Index of Buffer
    uint8_t Total;     // Total Data Bytes
    uint8_t Depth;     // length of the buffer,
    uint8_t PreCount;  // Preset value when counter is triggered
    void* List_p;      // Pointer for Data Matrix
} ScopeBFStr_t;

/**
 * @brief 資料寫入函式
 *
 * @param ScopeBF_p 傳入結構指標
 * @param Data_p 資料空間指標
 * @param Bytes 欲寫入資料 bytes 數
 * @return uint8_t 總資料筆數
 */
int8_t ScopeBF_put(ScopeBFStr_t* ScopeBF_p, void* Data_p, uint8_t Bytes);

/**
 * @brief 觸發計數函式
 *
 * @param ScopeBF_p 傳入結構指標
 * @return uint8_t 總資料筆數
 */
int8_t ScopeBF_tri(ScopeBFStr_t* ScopeBF_p);

/**
 * @brief 資料讀出函式
 *
 * @param ScopeBF_p 傳入結構指標
 * @param Data_p 資料空間指標
 * @param Bytes 欲讀出資料 bytes 數
 * @return uint8_t 總資料筆數
 */
int8_t ScopeBF_get(ScopeBFStr_t* ScopeBF_p, void* Data_p, uint8_t Bytes);

/**
 * @brief 資料設滿函式
 *
 * @param ScopeBF_p 傳入結構指標
 * @return uint8_t 總資料筆數
 */
uint8_t ScopeBF_full(ScopeBFStr_t* ScopeBF_p);

/**
 * @brief 資料清空函式，供前級工作呼叫以清空緩衝區。
 *
 * @param ScopeBF_p 傳入結構指標
 * @return uint8_t 總資料筆數
 */
uint8_t ScopeBF_clr(ScopeBFStr_t* ScopeBF_p);
/* Public Section End */
#endif  // C4MLIB_DATABUFFER_SCOPEBUFFER_H
