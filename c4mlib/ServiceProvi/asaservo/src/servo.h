/**
 * @file servo.h
 * @author 葉子哇 (w8indow61231111@gmail.com)
 * @date 2020.11.27
 * @brief 
 * 
 */

#ifndef C4MLIB_ASASERVO_SERVO_H
#define C4MLIB_ASASERVO_SERVO_H
#    ifdef __AVR_ATmega128__
#        include "m128/servo.h"
#    endif
#endif  //C4MLIB_ASASERVO_SERVO_H
